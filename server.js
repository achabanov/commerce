// Require the Express Module
var express = require('express');
// Create an Express App
var app = express();
// Require body-parser (to receive post data from clients)
var bodyParser = require('body-parser');
// Integrate body-parser with our App

// Set up mongoose
var mongoose = require('mongoose');
// Require path
var path = require('path');

// autoIn
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection('mongodb://localhost/commerce');
autoIncrement.initialize(connection);


// Setting our Static Folder Directory
app.use(express.static(path.join(__dirname, "commerce", 'dist')));
app.use(bodyParser.json());


app.use(bodyParser.urlencoded({ extended: true }));
mongoose.connect('mongodb://localhost/commerce');
mongoose.Promise = global.Promise;

var ProductSchema = new mongoose.Schema({
    name: {type: String, minlength: 3},
    qty: {type: Number, minimum: 0},
    price: {type: Number, minimum: 0}
})
mongoose.model('Product', ProductSchema);
var Product = mongoose.model('Product');


//Auto-increment
ProductSchema.plugin(autoIncrement.plugin, { model: 'Product' });
module.exports = mongoose.model('Product', ProductSchema);



app.post('/getproducts', function(req, res){
    console.log('inside post')
    console.log(req.body)
    var newProduct = new Product({name: req.body.name, qty: req.body.qty, price: req.body.price});
    newProduct.save(function(err){
        if(err){
            res.json(err);
        }else{
            res.json(newProduct);
        }
    })
})

app.get('/getproducts', (req, res) => {
    Product.find({}, (err, foundProducts)=>{
        if(err) {
            res.json(err);
        }else {
            res.json(foundProducts);
        }
    })
})

app.put('/getproducts/:id', (req, res)=>{
    Product.findOne({_id: req.params.id}, (err, foundProduct)=>{
        if(err) {
            res.json(err);
        }else {
            foundProduct.name = req.body.name;
            foundProduct.qty = req.body.qty;
            foundProduct.price = req.body.price;
            foundProduct.save((err)=>{
                if(err) {
                    res.json(err);
                }else {
                    res.json(foundProduct);
                }
            })
        }
    })
});


app.get('/getproducts/:id',(req, res)=>{
    Product.findOne({_id: req.params.id}, (err, foundProduct)=>{
        if(err) {
            res.json(err);
        }else {
            res.json(foundProduct);
        }
    })
});


// app.post('/quotes/:product_id', (req,res)=>{
//     Product.findOne({_id: req.params.product_id}, (err,foundProduct)=>{
//         if(err) {
//             res.json(err);
//         }else {
//             console.log(foundProduct)
//             foundProduct.quotes.push({content: req.body.content});
//             foundAuthor.save((err)=>{
//                 if(err) {
//                     res.json(err);
//                 }else {
//                     res.json(foundAuthor);
//                 }
//             })
//         }
//     })
// })

app.delete('/getproducts/:id', (req,res)=>{
    Product.remove({_id: req.params.id}, (err)=>{
        res.json({message: 'product deleted'});
    })
})

// app.post('/quotes/:product_id', (req,res)=>{
//     Product.findOne({_id: req.params.product_id}, (err,foundProduct)=>{
//         if(err) {
//             res.json(err);
//         }else {
//             console.log(foundProduct)
//             foundProduct.quotes.push({content: req.body.content});
//             foundProduct.save((err)=>{
//                 if(err) {
//                     res.json(err);
//                 }else {
//                     res.json(foundProduct);
//                 }
//             })
//         }
//     })
// })


app.all("*", (req,res,next) => {
    res.sendFile(path.resolve("./commerce/dist/index.html"))
  });

// Setting our Server to Listen on Port: 8000
app.listen(8000, function() {
    console.log("listening on port 8000");
})