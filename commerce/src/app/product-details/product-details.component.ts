import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  id: string;
  product: any={name: "", qty: 0, price: 0};
  errors: string[] = [];
  constructor(private _httpService: HttpService, private _route: ActivatedRoute, private _router: Router ) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params)=>{
      console.log(params.get('id'));
      this.id = params.get('id');
      this._httpService.showProduct(this.id)
      .subscribe((data: object)=>{
        console.log(data);
        this.product = data;
      })
    })
  }
  delete(id){
    this._httpService.deleteProduct(id)
    .subscribe((data:any) => {
      console.log(data)
      // !!!!!!!!!!!!!!!!!!!!!!!CHECKING FOR ERRORS HERE!!!! REMEMBER 
      this.errors=[];
      if(data.errors){
        console.log(data)
        for (var key in data.errors){
          this.errors.push(data.errors[key].message);
          console.log(this.errors)
        }
      }else{
        console.log("lalalalal")
          this._router.navigate(['/products']);
        }
    })
  }
}