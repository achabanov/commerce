import { Component, OnInit } from '@angular/core';
import { HttpService } from './../http.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: object[]=[];
  constructor(private _httpService: HttpService) { }


  ngOnInit(){
    this.getAllProducts();
    console.log("data coming in ")
  }

  getAllProducts() {
    this._httpService.getProducts()
    .subscribe((data: object[])=>{
      console.log(data);
      this.products = data;
    })
  }
  delete(id){
    this._httpService.deleteProduct(id)
    .subscribe((data)=>{
      this.getAllProducts();
    })
  }

}
