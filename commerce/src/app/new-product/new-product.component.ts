import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {
  newProduct: any={name: "", qty: 0, price: 0};
  errors: string[] = [];

  constructor( private _httpService: HttpService, private _router: Router) { }

  ngOnInit() {
  }

  addProduct(){
    console.log('addProduct');
    console.log(this.newProduct)
    this._httpService.postProduct(this.newProduct)
    .subscribe((data:any) => {
      console.log(data)
      // !!!!!!!!!!!!!!!!!!!!!!!CHECKING FOR ERRORS HERE!!!! REMEMBER 
      this.errors=[];
      if(data.errors){
        for (var key in data.errors){
          this.errors.push(data.errors[key].message);
          console.log(this.errors)
        }
      }else{
          this._router.navigate(['/products']);
        }
    })
  }
}
