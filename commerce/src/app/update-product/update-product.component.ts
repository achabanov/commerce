import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  id: string;
  product: object = {name: ''};
  errors: string[] = [];
  constructor(private _httpService: HttpService, private _route: ActivatedRoute, private _router: Router ) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params)=>{
      console.log(params.get('id'));
      this.id = params.get('id');
      this._httpService.showProduct(this.id)
      .subscribe((data: object)=>{
        console.log(data);
        this.product = data;
      })
    })
  }
  editProduct(){
    this._httpService.updateProduct(this.product)
    .subscribe((data: any)=>{
      console.log(data);
      this.errors=[];
      if(data.errors){
        for (var key in data.errors){
          this.errors.push(data.errors[key].message);
          console.log(this.errors)
        }
      }else{
          this._router.navigate(['/']);
        }
    })
  }

}
