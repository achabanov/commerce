import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';




@Injectable()
export class HttpService {

  constructor(private _http: HttpClient) { }

  postProduct(productOb){
    console.log('postProduct');
    return this._http.post('/getproducts', productOb);
  }

  getProducts(){
    return this._http.get('/getproducts');
  }

  showProduct(id){
    return this._http.get('/getproducts/'+id);
  }

  deleteProduct(productId){
    return this._http.delete('/getproducts/' + productId);
  }

  updateProduct(productObj){
    return this._http.put('/getproducts/'+productObj._id, productObj)
  }
}
